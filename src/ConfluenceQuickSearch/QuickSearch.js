import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import AUIButton from 'aui-react/lib/AUIButton';
import AUIIcon from 'aui-react/lib/AUIIcon';
import AUIItemLink from 'aui-react/lib/AUIItemLink';
import AUIDropdownSection from 'aui-react/lib/AUIDropdownSection';
import AUIDropdownMenu from 'aui-react/lib/AUIDropdownMenu';

import classnames from 'classnames';

let availableItems = [];
let activeTargetName = '';
let activeContext = {};

export default class QuickSearch extends Component {
    constructor() {
        super();
        this._handleChange = this._handleChange.bind(this);
        this._handleKeyDown = this._handleKeyDown.bind(this);
        this._handleMouseUp = this._handleMouseUp.bind(this);
        this._handleNavigate = this._handleNavigate.bind(this);
        this._goTo = this._goTo.bind(this);
        this._handleFocus = this._handleFocus.bind(this);
        this._handleSearch = this._handleSearch.bind(this);
        this._clearActiveItem = this._clearActiveItem.bind(this);
        this.state = {
            interacting: false,
            activeItem: -1
        };
    }
    componentWillUnmount() {
        document.removeEventListener('mouseup', this._handleMouseUp, false);
        document.removeEventListener('keydown', this._handleKeyDown, false);
    }
    _setActiveItem(type, index, event) {
        this.setState({
            activeItem: availableItems.indexOf(`${type}.${index}`)
        });
    }
    _activateNextItem() {
        if(this.state.activeItem < availableItems.length - 1) {
            this.setState({
                activeItem: ++this.state.activeItem
            });
        }
    }
    _activatePreviousItem() {
        if(this.state.activeItem > -1) {
            this.setState({
                activeItem: --this.state.activeItem
            });
        }
    }
    _clearActiveItem() {
        this.setState({
            activeItem: -1
        });
    }
    _handleMouseUp(event) {
        if (event.target !== this.refs.searchQuery) {
            setTimeout(() => {
                this.setState({
                    interacting: false
                });
            }, 0);
        }
    }
    _getContext(result) {
        switch(result.className) {
            case 'content-type-page':
            case 'content-type-blogpost':
                return {
                    contentId: result.id
                };
            case 'content-type-attachment-image':
                return {
                    contentId: Number(result.href.match(/preview=%2F([^%]+)/)[1]),
                    attachmentId: result.id
                };
            case 'search-for':
                return {
                    query: ''
                };
            case 'content-type-userinfo':
                return {
                    username: result.name
                };
        }
        throw new Error(`Content type "${result.className}" not supported`);
    }
    _goTo(targetName, context) {
        return this._handleNavigate.bind(this, targetName, context);
    }
    _handleNavigate(targetName, context, event) {
        switch(targetName) {
            case 'sitesearch':
                context = {
                    query: this.props.searchQuery
                };
                break;
        }

        const selectEvent = new CustomEvent('select', {
            detail: {
                targetName,
                context,
                nativeEvent: event
            },
            bubbles: true
        });

        ReactDOM.findDOMNode(this).dispatchEvent(selectEvent);
        if (this.props.onSelect) {
            this.props.onSelect(event, targetName, context);
        }
    }
    _getTargetName(sectionName) {
        switch(sectionName) {
            case 'search-for':
                return 'sitesearch';
            case 'recent-content':
            case 'content-type-page':
            case 'content-type-blogpost':
                return 'contentview';
            case 'content-type-attachment-image':
                return 'attachmentview';
            case 'content-type-userinfo':
                return 'userprofile';
        }

        return sectionName;
    }
    _resultsList({ results, anchorClass }) {
        return (
            <AUIDropdownSection key={anchorClass}>
                {results.map((result, index) => {
                    const activateCurrentPage = (targetName, context) => {
                        return () => {
                            activeTargetName = targetName;
                            activeContext = context;
                            this._setActiveItem(result.className, index);
                            this._userInteracting();
                        };
                    };

                    const classes = classnames({
                        'active': `${anchorClass}.${index}` === availableItems[this.state.activeItem]
                    });

                    const targetName = this._getTargetName(anchorClass);

                    return (
                        <AUIItemLink
                            key={index}
                            href={result.href}
                            className={classes}
                            anchorClassName={anchorClass}
                            onClick={this._goTo(targetName, result.context)}
                            onFocus={activateCurrentPage(targetName, result.context)}
                            onBlur={this._clearActiveItem}
                            onMouseOver={activateCurrentPage(targetName, result.context)}
                            onMouseOut={this._clearActiveItem}
                            target="_top"
                        >
                            <span dangerouslySetInnerHTML={{ __html: result.name}} />
                            { result.spaceName ? <small>{result.spaceName}</small> : null }
                        </AUIItemLink>
                    );
                })}
            </AUIDropdownSection>
        );
    }
    _dropdownMenusBySection({ sections }) {
        availableItems = Object.keys(sections).reduce((context, sectionName) => {
            return context.concat(
                sections[sectionName].map((item, index) => {
                    return `${sectionName}.${index}`;
                })
            );
        }, []);

        return (
            <AUIDropdownMenu style={{display:'block'}}>
                {Object.keys(sections).map(sectionName =>
                    this._resultsList({ results: sections[sectionName], anchorClass: sectionName })
                )}
            </AUIDropdownMenu>
        );
    }
    _handleChange(event) {
        this._userInteracting();
        this.props.onChangeSearchQuery(event.target.value);
    }
    _userInteracting() {
        this.setState({
            interacting: true
        });
        document.addEventListener('mouseup', this._handleMouseUp, false);
        document.addEventListener('keydown', this._handleKeyDown, false);
    }
    _userStoppedInteracting() {
        this.setState({
            interacting: false,
            activeItem: -1
        });
        document.removeEventListener('mouseup', this._handleMouseUp, false);
        document.removeEventListener('keydown', this._handleKeyDown, false);
    }
    _handleKeyDown(event) {
        const ESC = 27;
        const DOWN_ARROW = 40;
        const UP_ARROW = 38;
        const ENTER = 13;

        switch (event.keyCode) {
            case ESC:
                this._userStoppedInteracting();
                break;
            case DOWN_ARROW:
                this._activateNextItem();
                break;
            case UP_ARROW:
                this._activatePreviousItem();
                break;
            case ENTER:
                if (this.state.activeItem > -1) {
                    this._handleNavigate(activeTargetName, activeContext, event);
                }
                this._userStoppedInteracting();
                break;
        }

    }
    _handleFocus(event) {
        this._userInteracting();
        if (this.props.onFocusInput) {
            this.props.onFocusInput(event);
        }
    }
    _handleSearch(event) {
        this.props.onSearch(event, this.props.searchQuery);
        event.preventDefault();
    }
    render() {
        let dropdown;

        if (this.props.searchResults && this.props.searchResults.length > 0) {
            const sections = this.props.searchResults.reduce((prev, result) => {
                prev[result[0].className] = result.map(result => {
                    result.context = this._getContext(result);
                    return result;
                });
                return prev;
            }, {});
            dropdown = this._dropdownMenusBySection({ sections });
        } else {
            if (this.props.recentPages && this.props.recentPages.length > 0) {
                const sections = {
                    'recent-content': this.props.recentPages.map(recentPage => {
                        return {
                            id: recentPage.id,
                            spaceName: recentPage.space,
                            context: {
                                contentId: recentPage.id
                            },
                            name: recentPage.title,
                            className: 'recent-content',
                            href: recentPage.url
                        };
                    })
                };

                if (this.props.showMoreLink) {
                    sections['recently-viewed'] = [{
                        id: 0,
                        name: 'More recently viewed pages...',
                        className: 'recently-viewed',
                        href: '#'
                    }];
                }

                dropdown = this._dropdownMenusBySection({ sections });
            }
        }

        // TODO: Fix hard-coded content path (/confluence) here.
        return (
            <form className={classnames('aui', this.props.containerClass)} action="/confluence/dositesearch.action" method="get">
                <fieldset>
                    <label className="assistive">Quick Search</label>
                    <input
                        autoComplete="off"
                        title="Quick Search (/or g , g)"
                        className="text"
                        name="queryString"
                        ref="searchQuery"
                        type="text"
                        onChange={this._handleChange}
                        onClick={this._handleFocus}
                        onFocus={this._handleFocus}
                        placeholder={this.props.placeholder}
                        value={this.props.searchQuery}
                    />
                    {this.state.interacting ? dropdown : null}
                    <AUIButton onClick={this._handleSearch}>Search</AUIButton>
                </fieldset>
            </form>
        );
    }
}

QuickSearch.displayName = 'Search';
QuickSearch.propTypes = {
    /**
     * A CSS class name that will be applied to the root DOM node.
     */
    containerClass: PropTypes.string,
    /**
     * Triggered when focusing on the search input field.
     */
    onFocusInput: PropTypes.func,
    /**
     * Triggered when the user selects a search result.
     */
    onSelect: PropTypes.func,
    /**
     * Trigger when a search is performed. e.g. When clicking the Search button or pressing Enter.
     */
    onSearch: PropTypes.func,
    /**
     * Placeholder text to show in the search input field.
     */
    placeholder: PropTypes.string,
    /**
     * The current value of the search input field.
     */
    searchQuery: PropTypes.string,
    /**
     * Triggered whenever the search query changes.
     */
    onChangeSearchQuery: PropTypes.func,
    /**
     * The search results to display (grouped by section).
     */
    searchResults: PropTypes.array,
    /**
     * The recent pages to display.
     */
    recentPages: PropTypes.array,
    /**
     * Indicates if a link should be displayed to show more recently visited pages.
     */
    showMoreLink: PropTypes.bool
};
