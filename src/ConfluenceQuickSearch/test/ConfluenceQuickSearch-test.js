import React from 'react';
import ConfluenceQuickSearch from '../ConfluenceQuickSearch';

import { shallow, render } from 'enzyme';
import chai, { expect } from 'chai';
import chaiEnzyme from 'chai-enzyme';

chai.use(chaiEnzyme());

import sinon, { spy } from 'sinon';
import sinonChai from 'sinon-chai';
chai.use(sinonChai);

const navigatorSpy = {
    go: spy()
};
const requestSpy = spy();

const requireMock = (moduleName, callback) => {
    switch(moduleName) {
        case 'navigator':
            return callback(navigatorSpy);
            break;
        case 'request':
            return callback(requestSpy);
            break;
    }
};

describe('ConfluenceQuickSearch', () => {
    beforeEach(() => {
        global.AP = {
            require: requireMock
        };
        sinon.spy(global.AP, 'require');
    });

    afterEach(() => {
        global.AP.require.restore();
        navigatorSpy.go.reset();
        requestSpy.reset();
    });

    it('should render a QuickSearch component', () => {
        const wrapper = shallow(<ConfluenceQuickSearch />);
        expect(wrapper).to.have.descendants('QuickSearch');
    });

    context('when changing search query', () => {
        it('should update the state with the new search query', () => {
            const componentMock = { setState: spy() };
            ConfluenceQuickSearch.prototype._handleSearchQueryChange.call(componentMock, 'test');
            expect(componentMock.setState).to.have.been.calledWith({ searchQuery: 'test' });
        });

        it('should trigger a search if the search query is longer than 1 character', () => {
            const componentMock = { setState: spy() };
            ConfluenceQuickSearch.prototype._handleSearchQueryChange.call(componentMock, 'test');
            expect(requestSpy).to.have.callCount(1);
        });

        it('should not trigger a search if the search query is shorter than 2 characters', () => {
            const componentMock = { setState: spy() };
            ConfluenceQuickSearch.prototype._handleSearchQueryChange.call(componentMock, 't');
            expect(requestSpy).to.have.callCount(0);
        });

        it('should clear the existing search results if the search query is shorter than 2 characters', () => {
            const componentMock = { setState: spy() };
            ConfluenceQuickSearch.prototype._handleSearchQueryChange.call(componentMock, 't');
            expect(componentMock.setState).to.have.been.calledWith({ searchResults: [] });
        });
    });

    context('when triggering a site search', () => {
        it('should call the navigator API with the search target and query', () => {
            const mockEvent = { preventDefault: () => {} };
            ConfluenceQuickSearch.prototype._handleSearch(mockEvent, 'my search');
            expect(navigatorSpy.go).to.have.callCount(1);
            expect(navigatorSpy.go).to.have.been.calledWith('sitesearch', { query: 'my search' });
        });

        it('should stop the default behaviour of the anchor click', () => {
            const mockEvent = { preventDefault: spy() };
            ConfluenceQuickSearch.prototype._handleSearch(mockEvent, 'my search');
            expect(mockEvent.preventDefault).to.have.callCount(1);
        });
    });

    //context('when selecting a result', () => {
    //    it('should use the Connect navigation API to go to the contentview page with the correct id', () => {
    //        const mockEvent = { preventDefault: () => {} };
    //        ConfluenceQuickSearch.prototype._handleContentItemSelect(mockEvent, 'contentview', { contentId: 12345 });
    //        expect(navigatorSpy.go).to.have.been.calledWith('contentview', { contentId: 12345 });
    //    });
    //
    //    it('should prevent the default behaviour of the event', () => {
    //        const mockEvent = { preventDefault: () => {} };
    //        sinon.spy(mockEvent, 'preventDefault');
    //        ConfluenceQuickSearch.prototype._handleContentItemSelect(mockEvent, 'contentview', { contentId: 12345 });
    //        expect(mockEvent.preventDefault).to.have.callCount(1);
    //    });
    //});
});
