import React, { Component, PropTypes } from 'react';
import { xdm_e } from '../context';
import { require } from '../facades/AP';
import { ContentBody } from 'confluence-react-components';
import { convertRelativeToAbsolute } from '../html';

export default class ConfluenceContentBody extends Component {
    constructor() {
        super();

        this.baseUrl = xdm_e();
        this.state = {
            hasRequested: false,
            content: {
                isSPAFriendly: false,
                body: '',
                cssDependencies: '',
                jsDependencies: []
            }
        };

        this._contentRequestHandler = this._contentRequestHandler.bind(this);
    }
    componentWillMount() {
        const contentId = this.props.contentId;

        if (!contentId) {
            return;
        }

        this._getContent(contentId);
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.contentId === this.props.contentId) {
            return;
        }

        this._getContent(nextProps.contentId);
    }

    _contentRequestHandler(response) {
        const metadata = JSON.parse(response).metadata;

        const parsedBodyView = JSON.parse(response).body.export_view;

        const body = convertRelativeToAbsolute(parsedBodyView.value, this.baseUrl);
        const cssDependencies = parsedBodyView.webresource.tags.css || '';
        const jsDependencies = parsedBodyView.webresource.uris.js || [];

        this.setState({
            content: {
                id: this.props.contentId,
                body,
                cssDependencies,
                jsDependencies,
                isSPAFriendly: metadata.spaFriendly
            },
            hasRequested: true
        });
    }

    _getContent(contentId) {
        require('request', request => {
            request({
                url: `/rest/api/content/${contentId}?expand=${Constants.EXPANSIONS}`,
                success: this._contentRequestHandler
            });
        });
    }

    render() {
        const { hasRequested, content} = this.state;

        if (!hasRequested) {
            return <div/>;
        }

        return (
            <ContentBody
                baseUrl={this.baseUrl}
                content={content}
            />);
    }
}

ConfluenceContentBody.displayName = 'ConfluenceContentBody';
ConfluenceContentBody.propTypes = {
    /**
     * The ID of the content to render.
     */
    contentId: PropTypes.string
};

export const Constants = {
    EXPANSIONS: 'body.export_view.webresource.tags.css,body.export_view.webresource.uris.js,metadata.spaFriendly'
};
