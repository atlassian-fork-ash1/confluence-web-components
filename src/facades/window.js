export function addEventListener() {
    window.addEventListener.apply(window, arguments);
}

export function removeEventListener() {
    window.removeEventListener.apply(window, arguments);
}
