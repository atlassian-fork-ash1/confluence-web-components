import React from 'react';

import sinon, { spy, stub } from 'sinon';
import sinonChai from 'sinon-chai';
import { shallow, render } from 'enzyme';
import chai, { expect } from 'chai';
import chaiEnzyme from 'chai-enzyme';
import proxyquire from 'proxyquire';

chai.use(chaiEnzyme());
chai.use(sinonChai);

import ConfluenceContentList from '../ConfluenceContentList';

const navigatorSpy = {
    go: spy()
};

const mockResponse = '{ "_links": { "base": "http://localhost", "context": "/confluence" }, "results": [{ "foo" : "bar" }] }';

const mockAP = {
    request: (args) => {
        args.success(mockResponse);
    }
};

sinon.spy(mockAP, 'request');

const requireMock = (moduleName, callback) => {
    switch(moduleName) {
        case 'navigator':
            return callback(navigatorSpy);
            break;
        case 'request':
            return callback(mockAP.request);
            break;
    }
};

describe('ConfluenceContentList', () => {
    beforeEach(() => {
        global.AP = {
            require: requireMock
        };
        sinon.spy(global.AP, 'require');
    });

    afterEach(() => {
        global.AP.require.restore();
        navigatorSpy.go.reset();
        mockAP.request.reset();
    });

    describe('Rendering', () => {
        it('should render an ol', () => {
            const wrapper = render(<ConfluenceContentList />);
            expect(wrapper).to.have.descendants('ol');
        });
    });

    describe('When component mounts', () => {
        it('should request the content if cql is provided', () => {
            const mockThis = {
                _getContent: spy(),
                props: {
                    cql: 'type=page'
                }
            };
            ConfluenceContentList.prototype.componentDidMount.call(mockThis);
            expect(mockThis._getContent).to.have.callCount(1);
            expect(mockThis._getContent).to.have.been.calledWith(mockThis.props.cql);
        });

        it('should not request the content if cql is not provided', () => {
            const mockThis = {
                _getContent: spy(),
                props: {}
            };
            ConfluenceContentList.prototype.componentDidMount.call(mockThis);
            expect(mockThis._getContent).to.have.callCount(0);
        });
    });

    describe('When component will receive properties', () => {
        it('should request the content if the cql has changed', () => {
            const mockThis = {
                _getContent: spy(),
                props: {
                    cql: 'type=page'
                }
            };
            ConfluenceContentList.prototype.componentDidMount.call(mockThis);
            ConfluenceContentList.prototype.componentWillReceiveProps.call(mockThis, { cql: 'type=space' });
            expect(mockThis._getContent).to.have.callCount(2);
            expect(mockThis._getContent).to.have.been.calledWith(mockThis.props.cql);
            expect(mockThis._getContent).to.have.been.calledWith('type=space');
        });

        it('should not request the content if the cql is the same', () => {
            const mockThis = {
                _getContent: spy(),
                props: {
                    cql: 'type=page'
                }
            };
            ConfluenceContentList.prototype.componentDidMount.call(mockThis);
            ConfluenceContentList.prototype.componentWillReceiveProps.call(mockThis, { cql: 'type=page' });
            expect(mockThis._getContent).to.have.callCount(1);
            expect(mockThis._getContent).to.have.been.calledWith(mockThis.props.cql);
        });
    });

    describe('When getting content', () => {
        it('should call the correct URL', () => {
            const mockThis = {
                props: {
                    limit: 25
                },
                setState: () => {}
            };
            ConfluenceContentList.prototype._getContent.call(mockThis, 'type=page');
            expect(mockAP.request).to.have.callCount(1);
            expect(mockAP.request.firstCall.args[0].url).to.equal('/rest/api/search?cql=type=page&limit=25');
        });

        it('should assign the response results to the component state', () => {
            const mockThis = {
                props: {
                    limit: 25
                },
                setState: spy()
            };
            ConfluenceContentList.prototype._getContent.call(mockThis, 'type=space');
            expect(mockThis.setState).to.have.been.calledWith({
                baseUrl: 'http://localhost',
                contextPath: '/confluence',
                content: [ { 'foo' : 'bar' }]
            });
        });
    });
});
