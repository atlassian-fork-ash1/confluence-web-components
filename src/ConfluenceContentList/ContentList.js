import React, { Component, PropTypes } from 'react';

export default class ContentList extends Component {
    render() {
        const { content, containerClass, itemClass, baseUrl } = this.props;
        return (
            <this.props.tagName className={containerClass}>
                {content.map(result => (
                    <li key={result.content.id} className={itemClass}>
                        <a href={`${baseUrl}${result.content._links.webui}`} target="_top">{result.content.title}</a>
                    </li>
                ))}
            </this.props.tagName>
        );
    }
}

ContentList.defaultProps = {
    tagName: 'ol'
};

ContentList.propTypes = {
    tagName: PropTypes.oneOf(['ul', 'ol']),
    containerClass: PropTypes.string,
    itemClass: PropTypes.string,
    content: PropTypes.array,
    baseUrl: PropTypes.string
};
